<?php
/*
 * Copyright (c) 2011, Jonathan Chan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 * following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace jonathanyc\lib\http;

abstract class Languages
{
    /**
     * Gets the languages specified in the HTTP Accept-Language header and their
     * associated q-factors.
     * @param array $accepted An array of languages we are willing to accept. If null (the default), all languages are
     * accepted.
     * @param string $default The language to default to if none is found.
     * @return array An associative array whose keys are supplied language values
     * and whose values are the supplied q-factors for each, ordered by highest
     * q-factor first.
     */
    public static function get(array $accepted = null, $default = 'en')
    {
        if (!isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            return array($default => 1);
        }

        // The raw languages, as supplied by the browser, come in this format:
        // en-ca,en;q=0.8,en-us;q=0.6,de-de;q=0.4,de;q=0.2
        $rawLanguages = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);

        // The "clean" languages, which are specified in $accepted and are all
        // lower-cased
        $languages = array();

        foreach ($rawLanguages as $languageValue) {
            $language = '';

            $languageValues = explode(';', $languageValue);

            // If the qfactor was not supplied
            if (count($languageValues) == 1) {
                // Lower-case the language
                $language = strtolower($languageValues[0]);
                // Default to a q-factor of one
                $qFactor = 1;
            // If the qfactor was supplied
            } else {
                $language = strtolower($languageValues[0]);
                $qFactor = $languageValues[1];

                // Validate the q-factor
                if (!is_numeric($qFactor)) {
                    // If it's not numeric (invalid) skip it
                    continue;
                }
            }

            // If this isn't one of the accepted languages, skip it.
            if ($accepted !== null && ! in_array($language, $accepted)) {
                continue;
            }

            // Add it to the languages array
            $languages[$language] = $qFactor;
        }

        // If we couldn't get any valid languages out, use the default
        if (count($languages) == 0) {
            return array($default => 1);
        } else {
            // Sort their languages by lowest q-factor first
            asort($languages, SORT_NUMERIC);

            // Reverse the array so we get the highest first and return
            return array_reverse($languages);
        }
    }
}