<?php
/*
 * Copyright (c) 2011, Jonathan Chan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 * following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace jonathanyc\lib\http\auth;

/**
 * A library for performing secure HTTP Digest authentication routines with PHP.
 *
 * For example usage, see http://icebrg.us/w/Lib.icebrg.us/http/auth/Digest.
 */
abstract class Digest
{
    /**
     * Builds the proper string for the WWW-Authenticate header based on the
     * challenge values provided.
     * @param array $challenge An associative array, whose keys and values map to
     * keys and values for the WWW-Authenticate header.
     * @return string A string appropriate for the value of the WWW-Authenticate
     * header.
     * @static
     */
    private static function _buildString(array $challenge)
    {
        $values = array();

        foreach ($challenge as $key => $value) {
            if ($value == '') {
                continue;
            }

            $values[] = $key . '="' . $value . '"';
        }

        return implode(',', $values);
    }

    /**
     * Generates a time-based integrity-guaranteed nonce value.
     * @param string $salt
     * @param string $time Defaults to time().
     * @return string A randomly generated nonce value.
     * @static
     */
    public static function genNonce($salt, $time = false)
    {
        if ($time === false) {
            $time = time();
        }

        return base_convert(time(), 10, 16) . ':' . hash_hmac('md5', time(), $salt);
    }

    /**
     * Sends a 401 Unauthorized status code and a WWW-Authenticate header with the
     * appropriate values, which can be overloaded in the $challenge array.
     * @param array $challenge An array of values to send in the WWW-Authenticate
     * header (they will be automatically formatted, via _build_string). For more
     * information on the specific values, see RFC 2617.
     * <table>
     *     <tr><th>Name</th><th>Default</th></tr>
     *     <tr><td>realm</td><td></td></tr>
     *     <tr><td>domain</td><td></td></tr>
     *     <tr><td>qop</td><td>auth</td></tr>
     *     <tr><td>nonce</td><td>gen_nonce($salt)</td></tr>
     *     <tr><td>opaque</td><td>uniqid()</td></tr>
     * </table>
     * @param string $salt The value to salt the nonce value with.
     * @return array The given $challenge array, combined with the $defaults.
     * @see _build_string
     * @static
     */
    public static function challenge(array $challenge, $salt)
    {
        $defaults = array(
                'realm'         =>    '',
                'domain'        =>    '',
                'qop'           =>    'auth,stale=true',
                'nonce'         =>    self::genNonce($salt),
                'opaque'        =>    uniqid() // We don't store the opaque values (for now), so
                // we don't really care
        );

        $challenge = array_merge($defaults, $challenge);

        header('HTTP/1.1 401 Unauthorized');
        header('Status: 401 Unauthorized');
        header('WWW-Authenticate: Digest ' . self::_buildString($challenge));

        return $challenge;
    }

    /**
     * @static
     * @return array|null An associative array of the parts of the Authorization header sent in the client's request,
     * or null.
     */
    public static function getRequestParts()
    {
        $req = array();

        // If there weren't any credentials supplied
        if (!isset($_SERVER['PHP_AUTH_DIGEST'])) {
            return null;
        }

        // Ignore empty parts (caused by padding)
        foreach (explode(',', $_SERVER['PHP_AUTH_DIGEST']) as $i => $value) {
            if (!empty($value)) {
                $value = trim($value);

                $parts = explode('=', $value, 2);
                $parts[1] = trim($parts[1], '"');

                $req[$parts[0]] = $parts[1];
            }
        }

        return $req;
    }

    /**
     * Attempts to authenticate using the HTTP Digest Authentication method,
     * checking credentials with $auth_func.
     * @param string $realm The realm that was sent to the user as part of the
     * WWW-Authenticate header, used here for verification.
     * @param string $salt The salt that was used for the call to challenge().
     * @param Closure $authFunc a Closure which, when called with the provided
     * $username, should return the stored HA1 string for that user. If no HA1
     * string is stored, the function should return false.
     * @param int $nonceTtl How many seconds old a nonce value should be before it
     * is considered stale.
     * @return string|bool The user's name if the credentials are correct, false
     * otherwise.
     * @see challenge
     * @static
     */
    public static function authenticate($realm, $salt, $authFunc, $nonceTtl = null)
    {
        $req = self::getRequestParts();

        if ($req == null) {
            return false;
        }

        // Validate the request (make sure it had all the valid parts)
        $requiredRequestParts = array(
            'username', 'realm', 'nonce', 'uri', 'response', 'opaque', 'qop', 'nc', 'cnonce'
        );

        foreach ($requiredRequestParts as $part) {
            if (!isset($req[$part])) {
                return false;
            }
        }

        // Make sure this authentication request is for the right resource
        if ($req['realm'] != $realm) return false;
        if ($req['uri'] != $_SERVER['REQUEST_URI']) return false;

        // Make sure the nonce value is valid - it must not be older than the
        // $nonce_ttl
        $nonceParts = explode(':', $req['nonce']);

        // Verify the second nonce part is the md5 of the first nonce part and the
        // $salt...
        $nonceTimestamp = base_convert($nonceParts[0], 16, 10);

        if ($nonceParts[1] != hash_hmac('md5', $nonceTimestamp, $salt)) {
            return false;
        }

        // Verify that the nonce is not too old... (if $nonce_ttl is not null)
        if ($nonceTtl !== null && $nonceTimestamp < time() - $nonceTtl) {
            return false;
        }

        // Get the HA1 value, using the supplied Closure
        $ha1 = $authFunc($req['username']);

        // If no HA1 value could be retrieved, we can't authenticate
        if ($ha1 === false) {
            return false;
        }

        $ha2 = md5(implode(':', array($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI'])));

        $expectedResponse = md5(implode(':', array($ha1, $req['nonce'], $req['nc'], $req['cnonce'], $req['qop'], $ha2)));

        if ($req['response'] != $expectedResponse) {
            return false;
        }

        return $req['username'];
    }
}