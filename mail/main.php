<?php
/*
 * Copyright (c) 2011, Jonathan Chan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 * following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace jonathanyc\lib\mail;

/**
 * Sends emails, with sensible default headers. Wraps PHP's built-in
 * <code>mail()</code> function.
 * @param string|array $from Can either be the email string or an array
 * consisting of the name and then the email address of the sender.
 * @param string|array $to Can either be the email string or an array
 * consisting of the name and then the email address of the sender.
 * @param string $subject The subject of the email.
 * @param string $message The message of the email.
 * @param array $headers An associative array of additional headers to send
 * which will be properly formatted in the email format.
 * @return bool True if the email is successfully registered for delivery, false
 * otherwise.
 */
function send($from, $to, $subject, $message, array $headers = null)
{
    // Default $headers_arr to an empty array
    if ($headers == null) {
        $headers = array();
    }

    // Format $from as a proper email string if it was specified as an array.
    // Ex: Jonathan Chan <jchan@icebrg.us>
    if (is_array($from) && count($from) == 2) {
        $from = $from[0] . ' <' . $from[1] . '>';
    }

    // Format $to as a proper email string if it was specified as an array.
    if (is_array($to) && count($to) == 2) {
        $to = $to[0] . ' <' . $to[1] . '>';
    }

    // The default email headers
    $defaultHeaders = array(
        'Reply-To'            =>    $from,
        'Return-Path'        =>    $from,
        'From'                    =>    $from,
        'Content-Type'    =>    'text/plain'
    );

    // Merge the user-supplied headers with the default headers, with the former
    // overriding the defaults.
    $headers = array_merge($defaultHeaders, $headers);

    // Construct the $headers string
    $headers = '';

    foreach ($headers as $key => $value) {
        $headers .= $key . ': ' . $value . "\r\n";
    }

    // Off we go!
    return mail($to, $subject, $message, $headers);
}