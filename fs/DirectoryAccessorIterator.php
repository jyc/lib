<?php
/*
 * Copyright (c) 2011, Jonathan Chan
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 * 
 *   - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 * following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace jonathanyc\lib\fs;

/**
 * An iterator implementation to implement over files and directories via a DirectoryAccessor.
 */
class DirectoryAccessorIterator implements \Iterator
{
    /**
     * The DirectoryAccessor whose files and directories we are to iterate over.
     * @var \jonathanyc\lib\fs\DirectoryAccessor
     */
    private $_da;

    /**
     * A directory handle representing the directory of the DirectoryAccessor.
     * @var \Directory
     */
    private $_dir;

    /**
     * The name of the current file or directory.
     * @var string
     */
    private $_currentName;

    /**
     * @param DirectoryAccessor $da The DirectoryAccessor whose files and directories we are to iterate over.
     */
    public function __construct(DirectoryAccessor $da)
    {
        $this->_da = $da;

        $this->_dir = dir($da->getDirName());

        $this->next();
    }

    public function __destruct()
    {
        $this->_dir->close();
    }

    public function current()
    {
        return $this->_da->get($this->_currentName);
    }

    public function key()
    {
        return $this->_currentName;
    }

    public function next()
    {
        $name = null;

        do {
            $name = $this->_dir->read();
        } while ($name === null || $name === '.' || $name === '..');

        $this->_currentName = $name;
    }

    public function rewind()
    {
        $this->_dir->rewind();

        $this->next();
    }

    public function valid()
    {
        if ($this->_currentName === false) {
            return false;
        }
        
        return true;
    }
}

/* End of File DirectoryAccessorIterator.php */