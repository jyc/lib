<?php
/*
 * Copyright (c) 2011, Jonathan Chan
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 * 
 *   - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 * following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace jonathanyc\lib\fs\tests;

set_include_path(get_include_path() . PATH_SEPARATOR . __DIR__ . '/../../../../');

require_once 'icebrg/lib/autoloader.php';
\jonathanyc\lib\autoloader\register();

use jonathanyc\lib\fs\DirectoryAccessor;

/**
 * Tests the DirectoryAccessor class's functionality.
 */
class DirectoryAccessorTestCase extends \PHPUnit_Framework_TestCase
{
    private static $_testdirContents = array('include.php', 'testfile');

    public function testAccess()
    {
        $da = new DirectoryAccessor(__DIR__ . '/data');

        $this->assertEquals(array('testdir'), $da->getChildNames());

        $this->assertEquals(self::$_testdirContents, $da->testdir->getChildNames());

        $this->assertEquals('Hello, world!', $da->testdir->testfile);
    }

    public function testAccessAsHandle()
    {
        $config = array(
            'accessFilesAs' =>  DirectoryAccessor::FILE_ACCESS_HANDLE,
            'fopenMode'     =>  'rb' // read binary
        );

        $da = new DirectoryAccessor(__DIR__ . '/data', $config);

        $this->assertEquals('Hello, world!', fgets($da->testdir->testfile));
    }

    public function testAccessAsHandleWithDynamicMode()
    {
        $config = array(
            'accessFilesAs'     =>  DirectoryAccessor::FILE_ACCESS_HANDLE,
            'defaultHandleMode' =>  'w'
        );

        $da = new DirectoryAccessor(__DIR__ . '/data', $config);

        $this->assertEquals('Hello, world!', fgets($da->testdir->readAs('rb')->testfile));
    }

    public function testArrayAccess()
    {
        $da = new DirectoryAccessor(__DIR__ . '/data');

        $this->assertEquals('Hello, world!', $da['testdir']['testfile']);
    }

    /**
     * @expectedException \RuntimeException
     * @return void
     */
    public function testExceptionOnNonExistentDirectory()
    {
        $da = new DirectoryAccessor(__DIR__ . '/nonexistent-dir');
    }

    /**
     * @expectedException \RuntimeException
     * @return void
     */
    public function testExceptionOnNonExistentDirectoryAccess()
    {
        $da = new DirectoryAccessor(__DIR__ . '/data');

        $da->nonexistent;
    }

    /**
     * @expectedException \RuntimeException
     * @return void
     */
    public function testExceptionOnNonExistentFileAccess()
    {
        $da = new DirectoryAccessor(__DIR__ . '/data');

        $da->testdir->nonexistent;
    }

    /**
     * @expectedException \RuntimeException
     * @return void
     */
    public function testExceptionOnDirectorySet()
    {
        $da = new DirectoryAccessor(__DIR__ . '/data');

        $da->testdir = "Should not work.";
    }

    public function testIsSetDirectory()
    {
        $da = new DirectoryAccessor(__DIR__ . '/data');

        $this->assertEquals(true, isset($da['testdir']['testfile']));

        $this->assertEquals(false, isset($da['nonexistent']));
        $this->assertEquals(false, isset($da['testdir']['nonexistent']));
    }

    public function testFileSet()
    {
        $da = new DirectoryAccessor(__DIR__ . '/data');

        $da['testdir']['testset.tmp'] = "Hello, again!";
        
        $this->assertEquals("Hello, again!", $da['testdir']['testset.tmp']);

        unlink(__DIR__ . '/data/testdir/testset.tmp');
    }

    public function testCount()
    {
        $da = new DirectoryAccessor(__DIR__ . '/data');

        $this->assertEquals(count(self::$_testdirContents), count($da->testdir));
    }

    public function testAccessAsRequire()
    {
        $config = array(
            'accessFilesAs' =>  DirectoryAccessor::FILE_ACCESS_REQUIRE
        );

        $da = new DirectoryAccessor(__DIR__ . '/data', $config);

        $expected = array(
            'data'  =>  "Hello, world!"
        );

        $this->assertEquals($expected, $da->testdir->{'include.php'});
    }
}

/* End of File DirectoryAccessorTest.php */