<?php
/*
 * Copyright (c) 2011, Jonathan Chan
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 * 
 *   - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 * following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace jonathanyc\lib\fs;

/**
 * Allows access of directories and the files inside of them as if the directory tree were an associative array.
 */
class DirectoryAccessor implements \ArrayAccess, \IteratorAggregate, \Countable
{
    /**
     * Indicates that the accessed files should be represented as the file's contents.
     */
    const FILE_ACCESS_STRING    =   0;

    /**
     * Indicates that the accessed files should be represented as a read handle pointing to the file.
     */
    const FILE_ACCESS_HANDLE    =   1;

    /**
     * Indicates that accessed files should be represented as the value returned by a require() call on them.
     */
    const FILE_ACCESS_REQUIRE   =   2;

    /**
     * The name of the directory this instance accesses.
     * @var string
     */
    private $_dirName;

    /**
     * Either self::FILE_ACCESS_STRING, self::FILE_ACCESS_HANDLE, or FILE_ACCESS_REQUIRE, indicating how accessed files
     * should be represented.
     * @var int
     */
    private $_accessFilesAs;

    /**
     * The mode argument to pass to fopen() when files are opened for reading.
     * @var string
     */
    private $_fopenReadMode;

    /**
     * The mode argument to pass to fopen() when files are opened for writing.
     */
    private $_fopenWriteMode;

    /**
     * @throws \RuntimeException If a directory with name $dirName does not exist.
     * @param string $dirName The name of the directory this instance should access.
     * @param array|null $config An array of configuration values.
     *      <table>
     *          <tr><th>Name</th><th>Default</th><th>Description</th></tr>
     *          <tr><td>accessFilesAs</td><td>self::FILE_ACCESS_STRING</td><td>How accessed files should be represented.</td></tr>
     *          <tr><td>fopenReadMode</td><td>The mode argument to pass to fopen() when files are opened for reading.</td></tr>
     *          <tr><td>fopenWriteMode</td><td>The mode argument to pass to fopen() when files are opened for writing.</td></tr>
     *      </table>
     */
    public function __construct($dirName = __DIR__, array $config = null)
    {
        if ($config == null) $config = array();

        $dirName = rtrim($dirName, '/') . '/';

        if ( ! file_exists($dirName)) {
            throw new \RuntimeException("Directory '$dirName' does not exist.");
        }

        $defaultConfig = array(
            'accessFilesAs'     =>  self::FILE_ACCESS_STRING,
            'fopenReadMode'     =>  'rb',
            'fopenWriteMode'    =>  'wb'
        );

        $config = array_merge($defaultConfig, $config);

        $this->_dirName = $dirName;
        $this->_accessFilesAs   =   $config['accessFilesAs'];
        $this->_fopenReadMode   =   $config['fopenReadMode'];
        $this->_fopenWriteMode  =   $config['fopenWriteMode'];
    }

    /**
     * Gets the appropriate representation of a file or directory.
     * @throws \RuntimeException If an I/O error occurs.
     * @param string $name The name of the file or directory to open.
     * @return DirectoryAccessor|\resource|string A DirectoryAccessor if $name is a directory, a resource if $name is
     * a file and self::FILE_ACCESS_HANDLE is set, or a string if $name is a file and self::FILE_ACCESS_STRING is set.
     */
    public function get($name)
    {
        $name = $this->_dirName . $name;

        if (is_dir($name)) {
            return new DirectoryAccessor($name,
                array(
                    'accessFilesAs'     =>  $this->_accessFilesAs,
                    'fopenReadMode'     =>  $this->_fopenReadMode,
                    'fopenWriteMode'    =>  $this->_fopenWriteMode,
                ));
        } elseif (is_file($name)) {
            switch ($this->_accessFilesAs) {
            case self::FILE_ACCESS_REQUIRE:
                return require($name);
            
            case self::FILE_ACCESS_HANDLE:
                $fh = @fopen($name, $this->_fopenReadMode);

                if ($fh === false) {
                    throw new \RuntimeException(
                        "Failed to open file '$name' with mode '{$this->_fopenReadMode}'.");
                }

                return $fh;

            // Default to FILE_ACCESS_STRING
            default:
                $data = @fread(@fopen($name, $this->_fopenReadMode), filesize($name));

                if ($data === false) {
                    throw new \RuntimeException(
                        "Failed to read from file '$name' with mode '{$this->_fopenReadMode}'.");
                }

                return $data;
            }
        }

        // not a directory or a file - won't read
        throw new \RuntimeException("No file or directory with the name '$name' exists.");
    }

    /**
     * Sets the contents of a file.
     * @throws \RuntimeException If an I/O error occurs or if there is a directory instead of a file at $name.
     * @param string $name The name of the file to write to.
     * @param string $value The data to write to the file.
     * @return void
     */
    public function setFile($name, $value)
    {
        $name = $this->_dirName . $name;

        if (file_exists($name) && ! is_file($name)) {
            throw new \RuntimeException("We can only write to files.");
        }

        $fh = @fopen($name, $this->_fopenWriteMode);

        if ($fh === false) {
            throw new \RuntimeException(
                "Failed to open file '$name' with mode '{$this->_fopenReadMode}'.");
        }

        $result = @fwrite($fh, $value);

        if ($result === false) {
            throw new \RuntimeException(
                "Failed to read from file '$name' with mode '{$this->_fopenReadMode}'.");
        }
    }

    /**
     * Checks whether a file or directory exists.
     * @param string $name The name of the file or directory.
     * @return bool Whether or not the file or directory exists.
     */
    public function exists($name)
    {
        $name = $this->_dirName . $name;

        return file_exists($name);
    }

    /**
     * Deletes a file or directory.
     * @throws \RuntimeException If the file or directory cannot be deleted.
     * @param string $name The name of the file or directory to delete.
     * @return void
     */
    public function delete($name)
    {
        $name = $this->_dirName . $name;

        if (is_dir($name)) {
            $success = @rmdir($name);
        } elseif (is_file($name)) {
            $success =  @unlink($name);
        } else {
            $success = false;
        }

        if ( ! $success) {
            throw new \RuntimeException("Failed to delete file '$name'.");
        }
    }

    /**
     * Returns a list of all the files in this directory.
     * @return array
     */
    public function getChildNames()
    {
        $array = array_filter(scandir($this->_dirName),
            function($element)
            {
                return $element != '.' && $element != '..';
            });

        // sort lowest to highest
        sort($array);

        return $array;
    }

    /**
     * Counts the number of files and directories in this directory.
     * @return int
     */
    public function count()
    {
        return count($this->getChildNames());
    }

    /**
     * Returns an iterator that can be used to iterate over the representations of the files and directories in this
     * directory.
     * @return DirectoryAccessorIterator
     */
    public function getIterator()
    {
        return new DirectoryAccessorIterator($this);
    }

    /**
     * @see get()
     * @param string $name
     * @return DirectoryAccessor|\resource|string
     */
    public function __get($name)
    {
        return $this->get($name);
    }

    /**
     * @see setFile()
     * @param string $name
     * @param string $value
     * @return void
     */
    public function __set($name, $value)
    {
        $this->setFile($name, $value);
    }

    /**
     * @see exists()
     * @param string $name
     * @return bool
     */
    public function __isset($name)
    {
        return $this->exists($name);
    }

    /**
     * Checks if a file with the given name (offset) exists.
     * @param string $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return $this->exists($offset);
    }

    /**
     * @see get()
     * @param string $offset
     * @return DirectoryAccessor|\resource|string
     */
    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    /**
     * @see setFile()
     * @param string $offset
     * @param string $value
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $this->setFile($offset, $value);
    }

    /**
     * @see delete()
     * @param string $offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        $this->delete($offset);
    }

    /**
     * @param string $mode The mode argument to pass to fopen() when opening files for reading.
     * @return DirectoryAccessor
     */
    public function setFopenReadMode($mode)
    {
        $this->_fopenReadMode = $mode;

        return $this;
    }

    /**
     * @param string $mode The mode argument to pass to fopen() when opening files for writing.
     * @return DirectoryAccessor
     */
    public function setFopenWriteMode($mode)
    {
        $this->_fopenWriteMode = $mode;

        return $this;
    }

    /**
     * @param int $mode self::FILE_ACCESS_STRING or self::FILE_ACCESS_HANDLE to respresent accessed files as a string
     * or a read handle, respectively.
     * @return DirectoryAccessor
     */
    public function setAccessFilesAs($mode)
    {
        $this->_accessFilesAs = $mode;

        return $this;
    }

    /**
     * @see setAccessFilesAs()
     * @param string $mode
     * @return DirectoryAccessor
     */
    public function accessAs($mode)
    {
        return $this->setAccessFilesAs($mode);
    }

    /**
     * @see setFopenReadMode()
     * @param string $mode
     * @return DirectoryAccessor
     */
    public function readAs($mode)
    {
        return $this->setFopenReadMode($mode);
    }

    /**
     * @see setFopenWriteMode()
     * @param string $mode
     * @return DirectoryAccessor
     */
    public function writeAs($mode)
    {
        return $this->setFopenWriteMode($mode);
    }

    /**
     * @return string The name of the directory this instance reads from.
     */
    public function getDirName() {
        return $this->_dirName;
    }
}

/* End of File DirectoryAccessor.php */