<?php
/*
 * Copyright (c) 2011, Jonathan Chan
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 * 
 *   - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 * following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace jonathanyc\lib\localization;

use jonathanyc\lib\http\Languages;

/**
 * A modern, extremely simplistic "implementation" of an internationalization table.
 */
class LocaleTable
{
    /**
     * The base language, from which messages will be translated.
     * @var string
     */
    private $_base;

    /**
     * An associative array of language names to translation tables, which are associative arrays of domains to phrase
     * tables, which are which are associative arrays of phrases in the base language to translated phrases.
     * 
     * <code>
     * array(
     *  'fr'    =>  array(
     *   'default'   =>  array(
     *       "Hello, world!" =>  "Bonjour, tout le monde!"
     *   ),
     *   'domain'   =>  array(
     *       "Hello, world!" =>  "Salut!"
     *   )
     *  ),
     *  'zh'    =>  array(
     *   'default'   =>  array(
     *       "Hello, world!" =>  "你好世界"
     *   )
     *  )
     * );
     * </code>
     * @var array[string]array[string]array[string]string
     */
    private $_table;

    /**
     * The currently selected language, to which messages will be translated.
     * @var string
     */
    private $_current;

    /**
     * @throws \InvalidArgumentException If $table is not an array or an object implementing \ArrayAccess.
     * @param string $base The base language, from which messages will be translated.
     * @param array $table An associative array of language names to translation tables, which are associative arrays of domains to phrase
     * tables, which are which are associative arrays of phrases in the base language to translated phrases.
     * <code>
     * array(
     *  'fr'    =>  array(
     *   'default'   =>  array(
     *       "Hello, world!" =>  "Bonjour, tout le monde!"
     *   ),
     *   'domain'   =>  array(
     *       "Hello, world!" =>  "Salut!"
     *   )
     *  ),
     *  'zh'    =>  array(
     *   'default'   =>  array(
     *       "Hello, world!" =>  "你好世界"
     *   )
     *  )
     * );
     * </code>
     */
    public function __construct($base, $table)
    {
        if ( ! (is_array($table) || $table instanceof \ArrayAccess)) {
            throw new \InvalidArgumentException("\$table must be an array or an object implementing \\ArrayAccess!");
        }

        $this->_base = $this->_current = $base;
        $this->_table = $table;
    }

    /**
     * Sets the current language, to which messages will be translated.
     * @param string|null $name
     * @return void
     */
    public function setLocale($name = null)
    {
        // Autodetect name of the language based on HTTP headers.
        if ($name == null) {
            $languages = Languages::get();

            for ($i = 0; $i < count($languages); $i++) {
                if (isset($this->_table[$languages[$i]])) {
                    break;
                }
            }

            $name = $languages[$i];
        }

        $this->_current = $name;
    }

    /**
     * Translates a message.
     * @param string $message The message to translate, in the base language.
     * @param string $domain The domain in which the phrase to translate resides.
     * @return string|null The translated message or null if there is none.
     */
    public function translate($message, $domain = 'default')
    {
        // If the language we're translating to the base language, we don't need to do anything :)
        if ($this->_current == $this->_base) {
            return $message;
        }

        if ( ! @isset($this->_table[$this->_current][$domain][$message])) {
            return null;
        }

        return $this->_table[$this->_current][$domain][$message];
    }

    /**
     * Shorthand access to translate().
     * @see translate()
     * @param string $message
     * @param string $domain
     * @return null|string
     */
    public function __invoke($message, $domain = 'default')
    {
        return $this->translate($message, $domain);
    }
}

/* End of File LocaleTable.php */