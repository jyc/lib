<?php
/*
 * Copyright (c) 2011, Jonathan Chan
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 * 
 *   - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 * following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace jonathanyc\lib\localization\tests;

use jonathanyc\lib\localization\LocaleTable;

class LocaleTableTestCase extends \PHPUnit_Framework_TestCase
{
    private static $_table = array(
            'fr'    =>  array(
                'default'   =>  array(
                    "Hello, world!" =>  "Bonjour, tout le monde!"
                ),
                'domain'   =>  array(
                    "Hello, world!" =>  "Salut!"
                )
            ),
            'zh'    =>  array(
                'default'   =>  array(
                    "Hello, world!" =>  "你好世界"
                )
            ));

    public function testTranslation()
    {
        $t = new LocaleTable('en', self::$_table);

        $t->setLocale('fr');

        // Test longhand
        $this->assertEquals("Bonjour, tout le monde!", $t->translate("Hello, world!"));
    }

    public function testTranslationShorthand()
    {
        $t = new LocaleTable('en', self::$_table);

        $t->setLocale('fr');

        // Test shorthand
        $this->assertEquals("Bonjour, tout le monde!", $t("Hello, world!"));
    }

    public function testNullTranslation()
    {
        $table = array();

        $t = new LocaleTable('en', $table);

        $t->setLocale('fr');

        $this->assertNull($t("Hello, world!"));
    }
}

/* End of File LocaleTableTestCase.php */