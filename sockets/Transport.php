<?php
/**
 * Created by Jonathan.
 * Date: 9/14/11
 * Time: 3:59 PM
 */
namespace jonathanyc\lib\sockets;

/**
 * Represents a stream-like data transport.
 */
interface Transport {

    /**
     * Should construct a new Transport with the supplied configuration key-value array.
     * @abstract
     * @param array $config
     * @throws \jonathanyc\lib\sockets\exceptions\TransportException
     */
    public function __construct(array $config);

    /**
     * Should attempt to read from the Transport.
     * @abstract
     * @param int $length The number of bytes to read from the Transport.
     * @return mixed The data read from the Transport.
     */
    public function read($length);

    /**
     * Should attempt to write to the Transport.
     * @abstract
     * @param mixed $data The data to write to the Transport.
     * @return void
     */
    public function write($data);

    /**
     * Should close the Transport.
     * @abstract
     * @return void
     */
    public function close();
}

/* End of File Transport.php */