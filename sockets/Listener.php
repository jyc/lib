<?php
/**
 * Created by Jonathan.
 * Date: 9/14/11
 * Time: 4:11 PM
 */
namespace jonathanyc\lib\sockets;

/**
 * Represents a stream-like "listener," which listens for incoming connections.
 */
interface Listener {

    /**
     * Should construct a new Listener with the supplied configuration key-value array.
     * @abstract
     * @param array $config
     */
    public function __construct(array $config);

    /**
     * Should perform an operation analogous to the socket_select() PHP function, but applied to instances of this
     * Transport type.
     * @static
     * @abstract
     * @param array $read The Transports to watch and return if a read will not block.
     * @param array $write The Transports to watch to see if a write will not block.
     * @param float|null $timeout The maximum number of seconds this method should block. Null if this method should
     * block indefinitely.
     * @return array[int]mixed An array containing three values - first, an array of readable transports, second, an
     * array of writable transports, and finally, a count of the readable and writable transports.
     */
    public function select(array $read, array $write, $timeout);

    /**
     * Should attempt to accept a new Transport of this Listener's type. Similar to PHP's socket_accept() function.
     * @abstract
     * @param float|null $timeout The maximum number of seconds this method should block before returning, or null to
     * block indefinitely.
     * @return Transport|null
     */
    public function accept($timeout = 0);

    /**
     * Should close this Listener.
     * @abstract
     * @return void
     */
    public function close();
}

/* End of File Listener.php */