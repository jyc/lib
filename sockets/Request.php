<?php
namespace jonathanyc\lib\sockets;

/**
 * A container object for holding data from a single "request" sent by a client to the server.
 */
class Request {

    /**
     * The client that sent the data.
     * @var ServerClient
     */
    private $_client;

    /**
     * The data that was sent.
     * @var mixed
     */
    private $_data;

    /**
     * Constructs a new Request instance.
     * @param ServerClient $client The client that sent the data.
     * @param mixed $data The data that was sent.
     */
    public function __construct(ServerClient $client, $data) {
        $this->_client = $client;
        $this->_data = $data;
    }
    
    /**
     * @return ServerClient The client that sent the data.
     */
    public function get_client() {
        return $this->_client;
    }

    /**
     * @return mixed The data that was sent.
     */
    public function get_data() {
        return $this->_data;
    }
}