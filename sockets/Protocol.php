<?php
namespace jonathanyc\lib\sockets;

/**
 * Essentially a handler for incoming data received by a server instance.
 */
abstract class Protocol {

    /**
     * @var Server
     */
    private $_server;

    /**
     * Called when a new client has connected to the server.
     * @param ServerClient $client The client that connected.
     * @return Response|array[int]Response|void The response to indicate what action the server should take.
     */
    public function connected(ServerClient $client) {
        
    }
    
    /**
     * Called when a client has disconnected from the server.
     * @param ServerClient $client The client that disconnected.
     * @return void
     */
    public function disconnected(ServerClient $client) {
        
    }

    /**
     * Called when a line has been received from a client. This method will be called for every non-empty chunk of data
     * delimited by the newline character.
     * @param Request $request The client request - the data has been rtrim()ed.
     * @return Response|array[int]Response|void The response to indicate what action the server should take.
     */
    public function line_received(Request $request) {

    }
    
    /**
     * Called when data has been received from a client.
     * @param Request $request The client request - the data has been rtrim()ed.
     * @return Response|array[int]Response|void The response to indicate what action the server should take.
     */
    public function data_received(Request $request) {
        
    }

    /**
     * Called when the server is shutting down, before ServerClient sockets have been closed.
     * @return void
     */
    public function server_shutdown() {
        
    }

    /**
     * @return Server $server The current server.
     */
    public function get_server() {
        return $this->_server;
    }

    /**
     * Used to inject the given $server who will call this instance.
     * @param Server $server The server to inject.
     * @return void
     */
    public function set_server(Server $server) {
        $this->_server = $server;
    }
}