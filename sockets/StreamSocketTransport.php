<?php
/**
 * Created by Jonathan.
 * Date: 9/14/11
 * Time: 4:16 PM
 */
namespace jonathanyc\lib\sockets;

use jonathanyc\lib\sockets\exceptions\StreamSocketTransportException;

/**
 * @throws exceptions\StreamSocketTransportException|\InvalidArgumentException
 * A Transport implementation that uses PHP's stream sockets.
 */
class StreamSocketTransport implements Transport {

    /**
     * The underlying stream.
     * @var \resource
     */
    private $_stream;

    /**
     * @throws \InvalidArgumentException Thrown if the required 'stream' configuration value is missing or not a stream
     * resource.
     * @param array $config
     * <table>
     *  <tr><th>Name</th><th>Default</th><th>Description</th></tr>
     *  <tr><td>stream</td><td>Required</td><td>The stream to wrap.</td></tr>
     * </table>
     * @see Transport::__construct()
     */
    public function __construct(array $config) {
        if ( ! isset($config['stream'])) {
            throw new \InvalidArgumentException("'stream' configuration value is required!");
        }

        if ( ! is_resource($config['stream'])) {
            throw new \InvalidArgumentException("'stream' configuration value must be a stream resource!");
        }

        $this->_stream = $config['stream'];
    }

    public function read($length) {
        $result = @fread($this->_stream, $length);

        if ($result === false) {
            throw new StreamSocketTransportException("An error occurred reading data!");
        }

        return $result;
    }

    public function write($data) {
        $result = @fwrite($this->_stream, $data);

        if ($result === false || self::bytes($data) != $result) {
            throw new StreamSocketTransportException("An error occurred writing data!");
        }
    }

    public function close() {
        @fclose($this->_stream);
    }

    /**
     * @return \resource The underlying stream.
     */
    public function get_stream() {
        return $this->_stream;
    }

    /**
     * Returns the number of bytes in the given $data.
     * @static
     * @param $data
     * @return int
     */
    private static function bytes($data) {
        $bytes = null;

        // if mbstring is loaded and is set to overload functions...
        if (extension_loaded('mbstring') && ((int)ini_get('mbstring.func_overload') & 2)) {
            $bytes = mb_strlen($data, 'latin1');
        } else {
            $bytes = strlen($data);
        }

        return $bytes;
    }
}

/* End of File StreamSocketTransport.php */