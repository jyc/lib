<?php
/**
 * Created by Jonathan.
 * Date: 9/14/11
 * Time: 4:01 PM
 */
namespace jonathanyc\lib\sockets\exceptions;

/**
 * Thrown if an error occurs in a Transport.
 */
class TransportException extends \RuntimeException {

}

/* End of File TransportException.php */