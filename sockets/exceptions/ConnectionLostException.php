<?php
namespace jonathanyc\lib\sockets\exceptions;

/**
 * An exception to be thrown if the connection with a socket is lost for some reason.
 */
class ConnectionLostException extends \RuntimeException {
    
}