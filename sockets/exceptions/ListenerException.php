<?php
/**
 * Created by Jonathan.
 * Date: 9/14/11
 * Time: 4:13 PM
 */
namespace jonathanyc\lib\sockets\exceptions;

/**
 * Thrown if an error occurs in a Listener.
 */
class ListenerException extends \RuntimeException {

}

/* End of File ListenerException.php */