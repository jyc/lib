<?php
namespace jonathanyc\lib\sockets\exceptions;

/**
 * An exception to be thrown if a Socket error occurs.
 */
class SocketsException extends \RuntimeException {

}