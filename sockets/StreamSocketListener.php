<?php
/**
 * Created by Jonathan.
 * Date: 9/14/11
 * Time: 5:00 PM
 */
namespace jonathanyc\lib\sockets;

use jonathanyc\lib\sockets\exceptions\StreamSocketListenerException;

/**
 * @throws exceptions\StreamSocketListenerException
 * A Listener implementation that uses PHP's stream sockets.
 */
class StreamSocketListener implements Listener {

    /**
     * The underlying stream.
     * @var \resource
     */
    private $_stream;

    /**
     * @throws \InvalidArgumentException Thrown if a required configuration value is missing.
     * @throws exceptions\StreamSocketListenerException Thrown if we fail to create the stream socket.
     * @param array $config
     * <table>
     *  <tr><th>Name</th><th>Default</th><th>Description</th></tr>
     *  <tr><td>init_string</td><td>Required</td><td>The connection string passed as the first argument to
     *      stream_socket_server().</td></tr>
     *  <tr><td>context</td><td>stream_context_get_default()</td><td>The context to use for the new server stream.</td></tr>
     * </table>
     */
    public function __construct(array $config) {
        $stream = null;
        $errno = null;
        $errstr = null;

        if ( ! isset($config['init_string'])) {
            throw new \InvalidArgumentException("'init_string' configuration value is required!");
        }

        if ( ! isset($config['context'])) {
            $config['context'] = stream_context_get_default();
        } elseif ( ! is_resource($config['context'])) {
            throw new \InvalidArgumentException("'context' must be a valid stream context resource!");
        }

        $stream = stream_socket_server(
            $config['init_string'], $errno, $errstr, STREAM_SERVER_BIND | STREAM_SERVER_LISTEN, $config['context']);

        if ($stream === false || ! stream_set_blocking($stream, 0)) {
            throw new StreamSocketListenerException(
                "Failed to create new stream socket with errno {$errno} and errstr '{$errstr}'!");
        }

        $this->_stream = $stream;
    }

    /**
     * @static
     * @throws exceptions\StreamSocketTransportException Thrown if anything is passed to the $read or $write arrays
     * that isn't a StreamSocketTransport, or if an error occurs during the stream_select() call.
     * @param array[int]StreamSocketTransport $read
     * @param array[int]StreamSocketTransport $write
     * @param float $timeout
     * @see Transport::select()
     * @return array
     */
    public function select(array $read, array $write, $timeout) {
        $get_streams = function($value) {
            if ( ! ($value instanceof StreamSocketTransport)) {
                throw new StreamSocketListenerException("Cannot select() on non-StreamTransport instances!");
            }

            /** @var $value StreamSocketTransport */

            return $value->get_stream();
        };

        $except = null;
        $microseconds = $timeout === null ? null : ($timeout - floor($timeout)) * 1000000;
        $result = null;

        $read_streams = array_map($get_streams, $read);
        $write_streams = array_map($get_streams, $write);

        $readable = array();
        $writable = array();

        // stream_select won't work if all arrays are empty
        if (count($read_streams) == 0 && count($write_streams) == 0) {
            return array(array(), array(), 0);
        }

        $result = @stream_select($read_streams, $write_streams, $except, $timeout, $microseconds);

        foreach ($read as $transport) {
            if (in_array($transport->get_stream(), $read_streams)) {
                $readable[] = $transport;
            }
        }

        foreach ($write as $transport) {
            if (in_array($transport->get_stream(), $write_streams)) {
                $writable[] = $transport;
            }
        }

        if ($result === false) {
            throw new StreamSocketListenerException("An error occurred while calling stream_select()!");
        }

        return array($readable, $writable, count($readable) + count($writable));
    }

    /**
     * @param float|null $timeout
     * @return StreamSocketTransport|null
     */
    public function accept($timeout = 0) {
        $stream = @stream_socket_accept($this->_stream, $timeout);

        //if ($stream !== false && ! stream_set_blocking($stream, 0)) {
        //    throw new StreamSocketListenerException("Failed to set new stream resource to non-blocking mode!");
        //}

        return $stream === false
                ? null
                : new StreamSocketTransport(array(
                        'stream'   =>  $stream));
    }

    public function close() {
        @fclose($this->_stream);
    }
}

/* End of File StreamSocketListener.php */