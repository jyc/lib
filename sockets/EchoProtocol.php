<?php
namespace jonathanyc\lib\sockets;

/**
 * A simple Protocol that echoes back to clients what they have sent and broadcasts their message along with their id
 * to all other connected clients. Disconnects clients if 'quit' is received.
 */
class EchoProtocol extends Protocol {

    public function connected(ServerClient $client) {
        return new Response($client, "Welcome to the EchoServer!\r\n");
    }

    public function line_received(Request $request) {
        $response = new Response($request->get_client());
        $data = $request->get_data();
        
        if ($data === 'quit') {
            $response->set_disconnect(true);
        } else {
            $response->set_data($data . "\r\n");
        }
        
        $broadcast = array($response);
        
        foreach ($this->get_server()->get_clients() as $client) {
            if ($client === $request->get_client()) {
                continue;
            }
            
            $id = $this->get_server()->get_id($client);
            $broadcast[] = new Response($client, $id . "> " . $data . "\r\n");
        }
        
        return $broadcast;
    }
}