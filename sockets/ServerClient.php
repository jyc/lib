<?php
namespace jonathanyc\lib\sockets;

use jonathanyc\lib\sockets\exceptions\ConnectionLostException;
use jonathanyc\lib\sockets\exceptions\TransportException;

/**
 * A container object for a client currently connected to the server.
 */
class ServerClient {

    /**
     * The stream this client is connected by.
     * @var Transport
     */
    private $_transport;

    /**
     * Constructs a new ServerClient instance.
     * @param \resource $stream The stream the client is connected by.
     */
    public function __construct(Transport $transport) {
        $this->_transport = $transport;
    }

    /**
     * Closes this instance's stream before being garbage collected.
     */
    public function __destruct() {
        $this->close();
    }

    /**
     * CLoses this instance's stream.
     * @return void
     */
    public function close() {
        $this->_transport->close();
    }

    /**
     * @return \resource The stream this client is connected by.
     */
    public function get_transport() {
        return $this->_transport;
    }

    /**
     * Attempts to read $length bytes of data from this client's stream.
     * @throws exceptions\ConnectionLostException if this client has disconnected (read failed).
     * @param int $length The number of bytes to read from this client's stream.
     * @return null|string $length bytes or less of data read from this client's stream, or null if no data could be
     * read.
     */
    public function read($length) {
        try {
            $data = $this->_transport->read($length);
        } catch (TransportException $e) {
            throw new ConnectionLostException();
        }
        
        return ($data === '')
                ? null
                : $data;
    }

    /**
     * Sends the given $data to this client.
     * @throws exceptions\ConnectionLostException if this client has disconnected (write failed).
     * @param mixed $data The data to send.
     * @return void
     */
    public function send($data) {
        try {
            $this->_transport->write($data);
        } catch (TransportException $e) {
            throw new ConnectionLostException();
        }
    }
}