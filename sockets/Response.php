<?php
namespace jonathanyc\lib\sockets;

/**
 * Container object for holding information on what action a server should take in response to a Request.
 */
class Response {

    /**
     * The client to act upon.
     * @var ServerClient
     */
    private $_destination;

    /**
     * The data to send.
     * @var mixed|null
     */
    private $_data;

    /**
     * Whether or not to disconnect the client.
     * @var bool
     */
    private $_disconnect = false;

    /**
     * Constructs a new Response instance.
     * @param ServerClient $destination The client to act upon.
     * @param mixed|null $data The data to send.
     */
    public function __construct(ServerClient $destination, $data = null) {
        $this->_destination = $destination;
        $this->_data = $data;
    }

    /**
     * @return bool Whether or not to disconnect the client.
     */
    public function get_disconnect() {
        return $this->_disconnect;
    }

    /**
     * @param bool $value Whether or not to disconnect the client.
     * @return void
     */
    public function set_disconnect($value) {
        $this->_disconnect = $value;
    }

    /**
     * @return ServerClient The client to act upon.
     */
    public function get_destination() {
        return $this->_destination;
    }

    /**
     * @return mixed|null The data to send.
     */
    public function get_data() {
        return $this->_data;
    }

    /**
     * @param mixed $value The data to send.
     * @return void
     */
    public function set_data($value) {
        $this->_data = $value;
    }
}