<?php
/*
 * Copyright (c) 2011, Jonathan Chan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 * following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace jonathanyc\lib;

abstract class Fs
{
    /**
     * The recursive equivalent of filmetime. Gets the highest/newest file
     * modification time of a file under the given $path.
     *
     * @see filemtime
     * @param string $path the path to search
     * @return int The UNIX timestamp of the highest/newest file modification time.
     */
    public static function getDirLatestModifiedTime($path)
    {
        if (!is_dir($path)) {
            return false;
        }

        $path = rtrim($path, '/') . '/';

        $last_modified_time = false;

        $files = self::getFiles($path);

        while ($file_name = array_shift($files)) {
            if (is_dir($file_name)) {
                foreach (self::getFiles($file_name) as $sub_file_name) {
                    $files[] = $file_name . '/' . $sub_file_name;
                }

                continue;
            }

            $mtime = filemtime($path . '/' . $file_name);

            if ($mtime > $last_modified_time) {
                $last_modified_time = $mtime;
            }
        }

        return $last_modified_time;
    }

    /**
     * Gets a list of all the files in the given directory. Should be
     * faster than glob()ing. Basically a wrapper for scandir().
     * @param string $dir The name of the directory to traverse.
     * @return array[int]string|bool A list of file names, relative to the given
     * directory, or false on failure.
     */
    public static function getFiles($dir)
    {
        $files = @scandir($dir);

        // If we failed to open the directory (it must not have been a directory!)
        if ($files === false) {
            return false;
        }

        // Filter the . and .. "pseudo-files"
        foreach ($files as $i => $file){
            if ($file == '.' || $file == '..') {
                unset($files[$i]);
                continue;
            }
        }

        return $files;
    }
}