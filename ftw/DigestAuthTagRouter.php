<?php
/*
 * Copyright (c) 2011, Jonathan Chan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 * following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace jonathanyc\lib\ftw;

use \jonathanyc\lib\http\auth\Digest;
use \jonathanyc\lib\exceptions\MissingArgumentException;

class DigestAuthTagRouter implements TagRouter {

    private $_http_method_storage_key;

    private $_realm;

    private $_domain;

    private $_salt;

    private $_auth_func;

    private $_failure_action;

    public function __construct(array $config = null) {
        if ($config === null) {
            $config = array();
        }

        $default_config = array(
            'http_method_storage_key'    =>    'digest_http_method_tags',
            'realm'                      =>    null,
            'domain'                     =>    '',
            'salt'                       =>    null,
            'auth_func'                  =>    null,
            'failure_action'             =>    null,
        );

        $config = array_merge($default_config, $config);

        // Check to make sure all required values are present.
        // If a value in the default config array is null, that means it is
        // required.
        foreach ($config as $key => $value) {
            if ($value === null) {
                throw new MissingArgumentException($key, "Required value '" . $key . "'"
                        . " missing!");
            }
        }

        if (!is_callable($config['auth_func'])) {
            throw new NamedArgumentException('auth_func', "The 'auth_func' must be"
                    . " callable!");
        }

        $this->_http_method_storage_key    =    $config['http_method_storage_key'];
        $this->_realm                                        =    $config['realm'];
        $this->_domain                                    =    $config['domain'];
        $this->_salt                                        =    $config['salt'];
        $this->_auth_func                                =    $config['auth_func'];
        $this->_failure_action                    =    $config['failure_action'];
    }

    public function get_tags() {
        return array('DAuth');
    }

    public function get_match(Ftw $ftw, $tag, array $occurrences) {
        $http = new HttpMethodTagRouter($this->_http_method_storage_key);

        $http_occurrences = array();
        $nonce_ttls = array();

        // First, extract all of the HttpMethodTagRouter compatibles
        foreach ($occurrences as $file => $line_parts) {
            // Expect three parts: the HTTP method, the path, and the nonce TTL
            if (count($line_parts) !== 3) {
                // Skip if there are the wrong number of parts
                continue;
            }

            // HTTP Method (Tag) = $line_parts[0]
            // HTTP path = $line_parts[1]
            // Nonce TTL = $line_parts[2]

            $http_occurrences[$line_parts[0]][$file][] = $line_parts[1];
            $nonce_ttls[$line_parts[0]][$file] = $line_parts[2];
        }

        if (count($http_occurrences) == 0) {
            return false;
        }

        foreach ($http_occurrences as $tag => $file) {
            $result = $http->get_match($ftw, $tag, $file);

            if ($result !== false) {
                list($match, $http_args) = $result;

                $nonce_ttl = $nonce_ttls[$tag][$match];

                $user = Digest::authenticate(
                        $this->_realm, $this->_salt, $this->_auth_func, $nonce_ttl);

                if ($user !== false) {
                    $args = array(
                        'auth'    =>    array(
                            'username'    =>    $user
                        )
                    );

                    $args = array_merge($args, $http_args);

                    return array($match, $args);
                } else {
                    Digest::challenge(array(
                        'realm'        =>    $this->_realm,
                        'domain'    =>    $this->_domain
                    ), $this->_salt);

                    return array($this->_failure_action, array());
                }
            }
        }
    }
}