<?php
/*
 * Copyright (c) 2011, Jonathan Chan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 * following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace jonathanyc\lib\ftw;

use \jonathanyc\lib\cache\CacheStore;
use \jonathanyc\lib\exceptions\MissingArgumentException;
use \jonathanyc\lib\Fs;
use \jonathanyc\lib\Registry;

class Ftw {
    /**
     * The current Singleton instance of this class.
     * @see Ftw::get_instance()
     * @var Ftw
     */
    private static $_instance;

    ///////////////////
    // Configuration //
    ///////////////////

    /**
     * @see Ftw::__construct()
     * @var string
     */
    private $_base_uri;

    /**
     * @see Ftw::__construct()
     * @var string
     */
    private $_actions_dir;

    /**
     * @see Ftw::__construct()
     * @var string
     */
    private $_common_file_name;

    /**
     * @see Ftw::__construct()
     * @see Ftw::get_cache()
     * @var \jonathanyc\lib\cache\CacheStore
     */
    private $_cache_store;

    /**
     * @see Ftw::__construct()
     * @see \jonathanyc\lib\ftw\TagRouter
     * @var array[int]\jonathanyc\lib\ftw\TagRouter
     */
    private $_tag_routers;

    /**
     * @see Ftw::__construct()
     * @see \is_callable()
     * @var mixed
     */
    private $_error_handler;

    ///////////////////////
    // End Configuration //
    ///////////////////////

    /**
     * The tidied version of $_SERVER['REQUEST_URI'].
     * @see Ftw::get_uri()
     * @var string
     */
    private $_uri;

    /**
     * All of the tag data lines, sorted by file and then by tag name.
     * An array of arrays, keyed by tag names. The sub-arrays are keyed by file
     * name, and contain lists of tag data lines.
     * @var array[string]array[string]string
     */
    private $_tags;

    /**
     * An array of all the locations of common files.
     * @var array[int]string
     */
    private $_common_files;

    /**
     * The UNIX timestamp for when the actions directory was last modified.
     * @see Ftw::get_actions_mtime()
     * @var int
     */
    private $_actions_mtime;

    /**
     * A container for values exposed via common files.
     * @var \jonathanyc\lib\Registry
     */
    private $_common;

    private function __clone() { }
    private function __wakeup() { }

    /**
     * Called by {@link Ftw::get_instance()}.
     * @param array $config The configuration values.
     * <table>
     *    <tr><th>Name</th><th>Default Value</th><th>Description</th></tr>
     *    <tr><td>base_uri</td><td>/</td><td>The base URI from which this
     *            application is supposed to be executing from. Stripped from the
     *            beginning of all application URIs.</td></tr>
     *    <tr><td>actions_dir</td><td>actions</td><td>The directory in which
     *            "action" files are located. Action files are parsed for tags, which
     *            are handled by TagRouters registered to deal with them.</td></tr>
     *    <tr><td>common_file_name</td><td>__common</td><td>The name shared by all
     *            common files. Common files are automatically loaded when actions in
     *            their directory or descendent directories are run.</td></tr>
     *    <tr><td>tag_routers</td><td><code>array(new HttpMethodTagRouter())</code>
     *            </td><td>An array of TagRouter implementations, called for every
     *            different tag that is to be parsed.</td></tr>
     *    <tr><td>cache_store</td><td><em>none</em></td><td>A CacheStore
     *            implementation, used by FTW and by its components to cache values for
     *            fast access.</td></tr>
     *    <tr><td>error_handler</td><td><em>none</em></td><td>An
     *            is_callable() value (closure, lambda, functor) that must accept three
     *            arguments, the third optional. To be called when an error occurs in
     *            action execution, or when a value is returned from an action other
     *            than 404. The three arguments are, in order, the instance of Ftw, the
     *            error code (a HTTP status code), and an optional Exception parameter.
     *            </td></tr>
     *    <tr><td>common</td><td><code>array()</code></td><td>An array of common
     *            values to merge with the loaded common values.</td></tr>
     * </table>
     * @see Ftw::get_instance()
     */
    private function __construct(array $config = null) {
        // Default $config to an empty array, so array_merge will work.
        if ($config === null) {
            $config = array();
        }

        // The default configuration.
        $default_config = array(
            'base_uri'                    =>    '/',
            'actions_dir'                =>    'actions',
            'common_file_name'    =>    '__common',
            'tag_routers'                =>    array(
                new HttpMethodTagRouter()
            ),
            'common'                        =>    array()
        );

        // Merge the default config with the user-supplied config
            $config = array_merge($default_config, $config);

        // Verify $config['cache_store']
        if (!isset($config['cache_store'])
                || !($config['cache_store'] instanceof CacheStore)) {
            throw new MissingArgumentException('cache_store',
                    "'cache_store' is a required value, and it must be an implementation"
                    . " of \\icebrg\\lib\\cache\\CacheStore!");
        }

        // Verify $config['error_handler']
        if (!isset($config['error_handler'])
                || !is_callable($config['error_handler'])) {
            throw new \InvalidArgumentException(
                    "'error_handler' is a required value, and must be callable with"
                    . " three arguments - see the phpDoc for Ftw::get_instance().");
        }

        // Verify $config['actions_dir']
        if (!is_dir($config['actions_dir'])) {
            throw new \InvalidArgumentException(
                    "'actions_dir' must be a valid directory!");
        }


        // Verify $config['tag_routers']
        if (!is_array($config['tag_routers'])
                || count($config['tag_routers']) == 0) {
            throw new \InvalidArgumentException(
                    "'tag_routers' must contain at least one valid TagRouter!");
        }

        // Make sure each element in $config['tag_routers'] is really a TagRouter
        foreach ($config['tag_routers'] as $tag_router) {
            if (!($tag_router instanceof TagRouter)) {
                throw new \InvalidArgumentException(
                        "'tag_routers' must only contain implementations of TagRouter!");
            }
        }

        // Initialize $this->_common
        $this->_common = new Registry();

        // Add all of the starting common values to the common registry
        foreach ($config['common'] as $key => $value) {
            $this->_common->set($key, $value);
        }

        // Tidy up the actions_dir...
        $this->_actions_dir                = rtrim($config['actions_dir']) . '/';
        $this->_base_uri                    = $config['base_uri'];
        $this->_uri                                = $this->_get_tidy_uri();
        $this->_common_file_name    =    $config['common_file_name'] . '.php';
        $this->_cache_store                =    $config['cache_store'];
        $this->_tag_routers                =    $config['tag_routers'];
        $this->_error_handler            =    $config['error_handler'];

        // Get the last time the actions_dir was modified
        $this->_actions_mtime = Fs::getDirLatestModifiedTime($this->_actions_dir);

        // Load up all of the tags
        $this->_tags = $this->_load_all_tags();

        // Load up all of the $config['common_file_name'] files
        $this->_common_files = $this->_load_common_files();
    }

    /**
     * Gets the current Singleton instance of Ftw, instantiating a new one if
     * neccessary.
     * @param array $config The configuration array. See the constructor's
     * documentation for detailed information.
     * @return Ftw The current Singleton instance.
     * @see __construct()
     */
    public static function get_instance(array $config = null) {
        // If we don't already have an instance, construct one.
        if (!isset(self::$_instance)) {
            self::$_instance = new Ftw($config);
        }

        return self::$_instance;
    }

    /**
     * Loops through all of the tag handlers supplied in
     * {@link Ftw::get_instance()}, and executes the first matching action.
     * @return bool True if an action was executed, false otherwise.
     */
    public function execute() {
        $storage_keys = array('tags', 'common_files');

        // Call the $config['tag_routers'] on every tag, to see if we can get
        // a match
        foreach ($this->_tag_routers as $tag_router) {
            // Get all of the tag names that we have found and that this TagRouter
            // indicates it accepts
            $intersect_keys = array_intersect(array_keys($this->_tags), $tag_router->get_tags());

            // Iterate through each of the tag names
            foreach ($intersect_keys as $intersect_key) {
                // Call the get_match() method of the TagRouter, which will return the
                // name of the matching file and the arguments to extract() in it if one
                // is found, or false otherwise.
                $result = $tag_router->get_match(
                        $this, // Ftw
                        $intersect_key, // the tag name
                        $this->_tags[$intersect_key] // an array keyed by action names and
                        // valued by arrays of tag values
                );

                if ($result === false) {
                    continue;
                }

                // Destructure the return (if we're successful)
                list($matched, $args) = $result;

                $relevant_commons = $this->_get_relevant_common_files($matched);

                // Get the relevant common files, and include them
                $files_to_check = array_merge(
                        $relevant_commons,
                        array($matched));

                // Make sure all of the files we are going to be including are not
                // figments of our imagination...
                foreach ($files_to_check as $file_name) {
                    if (!file_exists($file_name)) {
                        // Purge the cache.
                        foreach ($storage_keys as $key) {
                            $this->_cache_store->delete($key);
                        }

                        // Call a 404.
                        $handler = $this->_error_handler;
                        $handler($this, 404);

                        return false;
                    }
                }

                // A sandbox for loading all of the stuff for each of the commons
                $common_files_loader = function() use ($relevant_commons) {
                    $common_arr = array();

                    foreach ($relevant_commons as $file_name) {
                        $sandbox = function() use ($file_name) {
                            return require $file_name;
                        };

                        $return = $sandbox();

                        if (is_array($return)) {
                            $common_arr = array_merge($return, $common_arr);
                        }
                    }

                    return $common_arr;
                };

                // Set the _common registry...
                foreach ($common_files_loader() as $key => $value) {
                    $this->_common->set($key, $value);
                }

                $sandbox = function() use ($matched, $args) {
                    extract($args);

                    return require $matched;
                };

                try {
                    $return = $sandbox();

                    // return() returns 1 on success - anything else is user-defined.
                    // Ignore null (if the user did an empty return) and true.
                    if ($return !== 1 && $return !== null && $return !== true) {
                        $handler = $this->_error_handler;
                        $handler($this, $return);
                    }

                    return true;
                } catch (\Exception $e) {
                    $handler = $this->_error_handler;
                    $handler($this, 500, $e);

                    return false;
                }
            }
        }

        // If we haven't gotten a single match so far, execute the $error_handler
        // and then return false.

        $handler = $this->_error_handler;
        $handler($this, 404);

        return false;
    }

    /**
     * Returns an array of all of the common files in the provided $action_name's
     * directory, and of parent directories.
     * @param string $action_name The file name of the action to inspect.
     * @return array[int]string An array of all the common files relevant to the
     * $action_name.
     */
    private function _get_relevant_common_files($action_name) {
        $relevant = array();

        $check = $action_name;
        while (true) {
            $check = dirname($check);

            if (isset($this->_common_files[$check])) {
                $relevant[] = $check . '/' . $this->_common_file_name;
            }

            if ($check == '/' || $check == '.') {
                return $relevant;
            }
        }
    }

    /**
     * Loads all of the tags from all of the files in the actions directory. Uses
     * a cached value, if possible.
     * @return array[string]array[string]string|bool An array of tag data lines,
     * sorted by file name, then sorted by tag name, or false on failure.
     */
    private function _load_all_tags() {
        $storage_key = 'tags';

        $tags = array();

        // If the cached tags are recent
        if ($this->_cache_store->mtime($storage_key) >= $this->_actions_mtime) {
            // save the trouble of loading them again and use them
            $tags = $this->_cache_store->get($storage_key);
        // Otherwise, something has changed, and we need to re-parse each file.
        } else {
            // Get all the files in $this->_actions_dir (this only gets the top-level
            // files - stay tuned)
            $files = glob($this->_actions_dir . '*');

            // While there is still a file at the beginning of the queue left to parse...
            while ($file_name = array_shift($files)) {
                // If the file is a directory,
                if (is_dir($file_name)) {
                    // add all of the files in it to end of the file queue
                    $files = array_merge($files, glob($file_name . '/*'));

                    continue;
                }

                // Otherwise, if it's just a regular file...

                // Skip non .php files - the @ is because substr_compare will print a
                // warning of the offset is out of the $file_name's bounds
                $comparison = @substr_compare($file_name, '.php', -4);

                // $comparison will be 0 if they are equal - anything else = no match
                if ($comparison !== 0) {
                    continue;
                }

                // Load the file's tags, in an associative array of arrays (the keys are
                // the tag names, the values are data associated with each ocurrence of
                // the tag
                $file_tags = self::load_file_tags($file_name);

                foreach ($file_tags as $tag => $lines) {
                    foreach ($lines as $line) {
                        $tags[$tag][$file_name] = $line;
                    }
                }
            }

            // Save to the cache
            $this->_cache_store->put($storage_key, $tags);
        }

        return $tags;
    }

    /**
     * Loads an array of all of the common files in the actions directory. Uses
     * a cached value, if possible.
     * @return array[int]string A list of all of the files with the common file
     * name in the actions directory.
     * @see Ftw::_load_all_tags()
     */
    private function _load_common_files() {
        $storage_key = 'common_files';

        $common_files = array();

        // If the cached common_files are recent
        if ($this->_cache_store->mtime($storage_key) >= $this->_actions_mtime) {
            // just use them and save the trouble
            $common_files = $this->_cache_store->get($storage_key);
        // Otherwise, re-find all of the common_files
        } else {
            $files = glob($this->_actions_dir . '*');

            while ($file_name = array_shift($files)) {
                if (is_dir($file_name)) {
                    $files = array_merge($files, glob($file_name . '/*'));

                    continue;
                }

                $comparison = @substr_compare($file_name, '.php', -4);

                if ($comparison !== 0) {
                    continue;
                }

                if (basename($file_name) == $this->_common_file_name) {
                    $common_files[dirname($file_name)] = true;
                }
            }

            // Reverse the array, so the most general commons are loaded first
            $common_files = array_reverse($common_files, true);

            // Store in the cache
            $this->_cache_store->put($storage_key, $common_files);
        }

        return $common_files;
    }

    /**
     * Loads all of the tags and their data lines from the provided $file_name.
     * @param string $file_name The name of the file to load tags from.
     * @return array[string]array[int]string|bool An array of tag data lines,
     * sorted by tag name, or false on failure.
     */
    public static function load_file_tags($file_name) {
        $tags = array();
        // Open the file in binary reading mode
        $fh = @fopen($file_name, 'rb');

        if ($fh === false) {
            return false;
        }

        // Skip the first line (the one with <?php) - at the same time, check if
        // this file validly starts with <?php (it shouldn't be sending any output
        $first = rtrim(@fgets($fh));
        if ($first != "<?php") {
            return false;
        }

        // While we can read another line that starts with #$...
        while (($line = rtrim(fgets($fh))) && strpos($line, '#$') === 0) {
            $parts = array();

            // Split the line into pieces by ' '
            $line_parts = explode(' ', $line);

            // The tag is the 2nd element (the 1st element is the #$)
            $tag = $line_parts[1];

            // The rest is the tag data
            $data = array_splice($line_parts, 2);

            $tags[$tag][] = $data;
        }

        // Close the file
        fclose($fh);

        return $tags;
    }

    private function _get_tidy_uri() {
        $uri = $_SERVER['REQUEST_URI'];

        // Trim off the query string from the supplied path - the extra "-1" is because
        // QUERY_STRING does not include the ? character.
        if (!empty($_SERVER['QUERY_STRING'])) {
            $uri = substr($uri, 0, -1 * strlen($_SERVER['QUERY_STRING']) - 1);
        }

        $uri = trim($uri, '/');

        if (substr_compare($uri, $this->_base_uri, 0, strlen($this->_base_uri)) === 0) {
            $uri = substr($uri, strlen($this->_base_uri));
        }

        return $uri;
    }


    public function get_uri() {
        return $this->_uri;
    }

    public function get_cache() {
        return $this->_cache_store;
    }

    public function get_actions_mtime() {
        return $this->_actions_mtime;
    }

    /**
     * A shorthand version of Ftw::get_instance()->get_common( ... )
     */
    public static function common() {
        $args = func_get_args();
        $longhand = new \ReflectionMethod(__CLASS__, 'get_common');

        return $longhand->invokeArgs(self::get_instance(), $args);
    }

    public function get_common() {
        if (func_num_args() == 1) {
            return $this->_common->get(func_get_arg(0));
        }

        $return = array();

        foreach (func_get_args() as $arg) {
            $return[] = $this->_common->get($arg);
        }

        return $return;
    }
}