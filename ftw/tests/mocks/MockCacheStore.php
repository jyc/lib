<?php
namespace jonathanyc\lib\ftw\tests\mocks;

require __DIR__ . '/../../../cache/CacheStore.php';

use \jonathanyc\lib\cache as cache;

class MockCacheStore implements cache\CacheStore {

    public function __construct(array $config = null) {

    }

    public function mtime($key) {
        return 0;
    }

    public function delete($key) {
        return true;
    }

    public function put($key, $data) {
        return true;
    }

    public function get($key) {
        return null;
    }
}
