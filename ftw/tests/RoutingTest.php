<?php
namespace jonathanyc\lib\ftw\tests;

//NON-WORKING
//
//require __DIR__ . '/../../fs.php';
//require __DIR__ . '/../main.php';
//require __DIR__ . '/mocks/MockCacheStore.php';
//
//use \jonathanyc\lib\ftw as ftw;
//
//class RoutingTest extends \PHPUnit_Framework_TestCase {
//
//    /**
//     * An associative array of assorted test data.
//     * @var array
//     */
//    public static $data;
//
//    public function setUp() {
//        self::$data = array(
//            'actions'    =>    array(
//                'one'    =>    __DIR__ . '/data/RoutingTest/actions/one.php',
//                'two'    =>    __DIR__ . '/data/RoutingTest/actions/two.php'
//            ),
//            'actions_dir'    =>    __DIR__ . '/data/RoutingTest/actions'
//        );
//    }
//
//    public function testLoadPaths() {
//        $expected_paths = array(
//            'GET'    =>    array(
//                array(
//                    array(),
//                    self::$data['actions']['one']
//                ),
//                array(
//                    array(':named_parameter'),
//                    self::$data['actions']['one']
//                ),
//                array(
//                    array('a', ':complex', 'named', ':parameter'),
//                    self::$data['actions']['two']
//                )
//            )
//        );
//
//        $paths = ftw\_load_paths(
//                new mocks\MockCacheStore(),
//                self::$data['actions_dir']);
//
//        $this->assertEquals($expected_paths, $paths);
//    }
//
//    /**
//     * @expectedException \InvalidArgumentException
//     */
//    public function testLoadPathsInvalidActionsDirectory() {
//        ftw\_load_paths(new mocks\MockCacheStore(), '/tmp/' . uniqid());
//    }
//
//    public function testMatchUri() {
//        $paths = array(
//            'GET'    =>    array(
//                array(
//                    array(),
//                    self::$data['actions']['one']
//                ),
//                array(
//                    array(':named_parameter'),
//                    self::$data['actions']['one']
//                ),
//                array(
//                    array('a', ':complex', 'named', ':parameter'),
//                    self::$data['actions']['two']
//                )
//            )
//        );
//
//        $this->assertEquals(
//                ftw\match_uri('POST', '/a/complex/named/parameter', $paths),
//                false);
//
//        $this->assertEquals(
//                array(
//                    array('complex' => 'complex', 'parameter' => 'parameter'),
//                    self::$data['actions']['two']),
//                ftw\match_uri('GET', '/a/complex/named/parameter', $paths));
//    }
//
//    public function testParseRoutes() {
//        $expected_one = array(
//            'GET'    =>    array(
//                array(),
//                array(':named_parameter')
//            )
//        );
//
//        $expected_two = array(
//            'GET'    =>    array(
//                array('a', ':complex', 'named', ':parameter')
//            )
//        );
//
//        $this->assertEquals(
//                $expected_one,
//                ftw\parse_routes(self::$data['actions']['one']));
//
//        $this->assertEquals(
//                $expected_two,
//                ftw\parse_routes(self::$data['actions']['two']));
//    }
//
//    /**
//     * @depends testLoadPaths
//     * @depends testMatchUri
//     */
//    public function testRoutingOutput() {
//        // FTW config
//        $config = array(
//            'actions_dir'    =>    self::$data['actions_dir'],
//            'cache_store'    =>    new mocks\MockCacheStore()
//        );
//
//        // Set up the fake HTTP request
//        $_SERVER['REQUEST_METHOD'] = 'GET';
//        $_SERVER['REQUEST_URI'] = '/';
//
//        // Start output buffering, so we can capture the output
//        ob_start();
//
//        ftw\execute($config);
//
//        // Capture the output
//        $output = ob_get_contents();
//        ob_end_clean();
//
//        // Dismantle the fake HTTP request
//        unset($_SERVER['REQUEST_METHOD']);
//        unset($_SERVER['REQUEST_URI']);
//
//        $this->assertEquals("Hello, world!", $output);
//    }
//
//    /**
//     * @depends testLoadPaths
//     * @depends testMatchUri
//     */
//    public function testComplexRoutingOutput() {
//        $complex = 'complex';
//        $parameter = 'parameter';
//
//        // FTW config
//        $config = array(
//            'actions_dir'    =>    self::$data['actions_dir'],
//            'cache_store'    =>    new mocks\MockCacheStore()
//        );
//
//        // Set up the fake HTTP request
//        $_SERVER['REQUEST_METHOD'] = 'GET';
//        $_SERVER['REQUEST_URI'] = '/a/' . $complex . '/named/' . $parameter;
//
//        // Start output buffering, so we can capture the output
//        ob_start();
//
//        ftw\execute($config);
//
//        // Capture the output
//        $output = ob_get_contents();
//        ob_end_clean();
//
//        // Dismantle the fake HTTP request
//        unset($_SERVER['REQUEST_METHOD']);
//        unset($_SERVER['REQUEST_URI']);
//
//        $this->assertEquals("A $complex named $parameter!", $output);
//    }
//
//    /**
//     * @depends testLoadPaths
//     * @depends testMatchUri
//     */
//    public function testRoutingNotFoundCallback() {
//        $match_not_found = function($method, $uri, $query_string) {
//            echo "OK!";
//        };
//
//        // FTW config
//        $config = array(
//            'actions_dir'    =>    self::$data['actions_dir'],
//            'cache_store'    =>    new mocks\MockCacheStore()
//        );
//
//        // Set up the fake HTTP request
//        $_SERVER['REQUEST_METHOD'] = 'GET';
//        $_SERVER['REQUEST_URI'] = '/non/existent/path';
//
//        // Start output buffering, so we can capture the output
//        ob_start();
//
//        ftw\execute($config, $match_not_found);
//
//        // Capture the output
//        $output = ob_get_contents();
//        ob_end_clean();
//
//        // Dismantle the fake HTTP request
//        unset($_SERVER['REQUEST_METHOD']);
//        unset($_SERVER['REQUEST_URI']);
//
//        $this->assertEquals("OK!", $output);
//    }
//}