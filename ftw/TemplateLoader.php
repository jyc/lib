<?php
/*
 * Copyright (c) 2011, Jonathan Chan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 * following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace jonathanyc\lib\ftw;

use \jonathanyc\lib\exceptions\NamedArgumentException;

class TemplateLoader {

    /**
     * The base path from which templates are loaded, prefixed to all template
     * names.
     */
    private $_path;

    /**
     * The suffix which is to be appended to all template names.
     */
    private $_suffix;

    /**
     * The base uri of the application.
     * @var string
     */
    private $_base_uri;

    /**
     * Constructs a new instance with the supplied $config values.
     * @param array $config An associative array of configuration values.
     * <table>
     *     <tr><th>Name</th><th>Short Description</th><th>Default</th></tr>
     *     <tr><td>path</th><td>The base template path</td><td></td></tr>
     *     <tr><td>suffix</td><td>The suffix for template files</td><td>.php</td
     *             </tr>
     *    <tr><td>base_uri</td><td>The base uri for the application</td><td></td>
     *            </tr>
     * </table>
     * @throws \jonathanyc\lib\exceptions\NamedArgumentException
     */
    public function __construct(array $config = null) {
        if ($config == null) $config = array();

        if (!isset($config['path'])) {
            $this->_path = '';
        } else {
            if (!is_dir($config['path'])) {
                throw new NamedArgumentException('path', "The supplied 'path'"
                        . " configuration value is not a valid directory!");
            }

            $this->_path = rtrim($config['path'], '/') . '/';
        }

        $this->_suffix = isset($config['suffix']) ? $config['suffix'] : '';
        $this->_base_uri = isset($config['base_uri']) ? rtrim($config['base_uri'], '/') . '/' : '';
    }

    /**
     * Loads the view with the given $name (with $this->_path prepended and
     * $this->_suffix appended) in a sandbox, with the keys in $data extracted to
     * variables. $this->_base_uri is made available to the templates as
     * $base_uri_exposed_name.
     * @param string $name The name of the view to load.
     * @param array $data Data that should be accessible to the view - the keys
     * are extracted to variables.
     * @param string $base_uri_exposed_name The name that $this->_base_ should be
     * exposed to views with.
     * @return mixed The return value of the view.
     * @throws \jonathanyc\lib\exceptions\NamedArgumentException
     */
    public function load($name, array $data = null,
            $base_uri_exposed_name = 'root') {
        if ($data == null) $data = array();

        $file_name = $this->_path . $name . $this->_suffix;

        if (!is_file($file_name)) {
            throw new NamedArgumentException('name', "The supplied view name does not"
                    . " map to a file!");
        }

        $data[$base_uri_exposed_name] = $this->_base_uri;

        $sandbox = function() use ($file_name, $data) {;

            extract($data);

            require $file_name;
        };

        return $sandbox();
    }
}