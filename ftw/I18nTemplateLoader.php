<?php
/*
 * Copyright (c) 2011, Jonathan Chan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 * following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace jonathanyc\lib\ftw;

use \jonathanyc\lib\exceptions\MissingArgumentException;
use \jonathanyc\lib\exceptions\NamedArgumentException;
use \jonathanyc\lib\Fs;
use \jonathanyc\lib\cache\CacheStore;

class I18nTemplateLoader extends TemplateLoader {

    const STORAGE_KEY = 'i18n';

    private $_i18n_root;

    private $_language;

    private $_data_key;

    private $_i18n_suffix;

    private $_i18n_mtime;

    /**
     *
     * @var \jonathanyc\lib\cache\CacheStore
     */
    private $_cache_store;

    /**
     * Constructs a new instance with the supplied $config values. Adds the
     * following configuration options:
     * <table>
     *    <tr><th>Name</th><th>Short Description</th><th>Default</th></tr>
     *    <tr><td>i18n_root</td><td>A directory in which .ini files with
     *            localization keys are placed, mirroring the template directory in a
     *            separate directory for each language (en, en-US)</td></td><td>i18n
     *            </td></tr>
     *    <tr><td>language</td><td>The language to load.</td><td>en</td></tr>
     *    <tr><td>data_key</td><td>The key to put all i18n data in for the view.
     *            </td><td>i18n</td></tr>
     *    <tr><td>i18n_suffix</td><td>The suffix to add to template names for their
     *            i18n files</td><td>.ini</td></tr>
     *    <tr><td>cache_store</td><td>The cache store to use to cache localization
     *            files</td><td><em>none</em></td></tr>
     * </table>
     * @see TemplateLoader::__construct
     * @param array $config
     */
    public function __construct(array $config) {
        parent::__construct($config);

        $default_config = array(
            'language'        =>    'en',
            'data_key'        =>    'i18n',
            'i18n_suffix'    =>    '.ini'
        );

        $config = array_merge($default_config, $config);

        if (!isset($config['i18n_root'])) {
            throw new MissingArgumentException('i18n_root', "The i18n_root"
                    . " configuration value is required!");
        }

        if (!isset($config['cache_store'])
                || !($config['cache_store'] instanceof CacheStore)) {
            throw new MissingArgumentException('cache_store', "The cache_store"
                    . " configuration value is required and must be an implementation of"
                    . " \\icebrg\\lib\\cache\\CacheStore!");
        }

        if (!is_dir($config['i18n_root'])) {
            throw new NamedArgumentException('i18n_root', "The i18n_root"
                    . " configuration value must be a valid directory!");
        }

        $config['i18n_root'] = rtrim($config['i18n_root'], '/') . '/';

        // Find all of the possible languages in the i18n_root
        $files = Fs::getFiles($config['i18n_root']);

        if ($files === false) {
            throw new \RuntimeException("Unable to retrieve any available"
                    . " languages in the supplied 'i18n_root' directory.");
        }

        // Make sure the language the user specified is even available
        if (!in_array($config['language'], $files)) {
            throw new \RuntimeException("The specified 'language' isn't available"
                    . " in the specified 'i18n_root'!");
        }

        $this->_language = $config['language'];
        $this->_i18n_root = $config['i18n_root'];
        $this->_data_key = $config['data_key'];
        $this->_i18n_suffix = $config['i18n_suffix'];
        $this->_cache_store = $config['cache_store'];

        $this->_i18n_mtime = Fs::getDirLatestModifiedTime($this->_i18n_root);
    }

    /**
     * A wrapper for TemplateLoader::load that loads the i18n file for the given
     * view into a variable named by $this->_data_key.
     * @see TemplateLoader::load
     * @param string $name
     * @param array $data
     * @param string $base_uri_exposed_name
     */
    public function load($name, array $data = null,
            $base_uri_exposed_name = 'root') {
        // Initialize $data to an empty array
        if ($data == null) {
            $data = array();
        }

        $i18n = array();

        // If the cache store value's modification time is newer than the
        // modification time of the whole i18n directory, then we can just use the
        // cache. If the last time we failed to get a proper value (or if the file
        // did not exist), we have stored false - this will keep us from either
        // re-loading from the cache or re-parsing until the file is modified.
        if ($this->_cache_store->mtime(self::STORAGE_KEY . $name) >= $this->_i18n_mtime) {
            $result = $this->_cache_store->get(self::STORAGE_KEY . $name);

            if ($result !== false) {
                $i18n = $result;
            }
        } else {
            // Load up the relevant localization file and pass it in as data
            $i18n_file_name = $this->_i18n_root . $this->_language . '/' . $name . $this->_i18n_suffix;

            if (file_exists($i18n_file_name)) {
                $result = parse_ini_file($i18n_file_name, true);

                if ($result !== false) {
                    $i18n = $result;

                    $this->_cache_store->put(self::STORAGE_KEY . $name, $i18n);
                } else {
                    // Store false, so we don't attempt to load i18n from either the
                    // cache or re-index it until it is modified
                    $this->_cache_store->put(self::STORAGE_KEY . $name, false);
                }
            } else {
                $this->_cache_store->put(self::STORAGE_KEY . $name, false);
            }
        }

        $data[$this->_data_key] = $i18n;

        parent::load($name, $data, $base_uri_exposed_name);
    }
}