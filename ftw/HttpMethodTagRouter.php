<?php
/*
 * Copyright (c) 2011, Jonathan Chan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 * following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace jonathanyc\lib\ftw;

class HttpMethodTagRouter implements TagRouter {

    private $_storage_key;

    public function __construct($storage_key = 'http_method_tags') {
        $this->_storage_key = $storage_key;
    }

    public function get_tags() {
        return array('GET', 'POST', 'PUT', 'DELETE');
    }

    public function get_match(Ftw $ftw, $tag, array $occurrences) {
        if ($tag != $_SERVER['REQUEST_METHOD']) {
            return false;
        }

        $storage_key = 'paths';
        $args = array();
        $match = false;

        $uri = $ftw->get_uri();

        // If the URI has a file extension, trim it off (the browser should be sending
        // the requested type via HTTP header
        $uri_info = pathinfo($uri);

        if (isset($uri_info['extension'])) {
            $uri = substr($uri, 0, -1 * (1 + strlen($uri_info['extension'])));
        }

        $uri_parts = ($uri == '') ? array() : explode('/', trim($uri, '/'));

        if ($ftw->get_cache()->mtime($this->_storage_key . $tag) >= $ftw->get_actions_mtime()) {
            $files_map = $ftw->get_cache()->get($this->_storage_key . $tag);
        } else {
            $files_map = array();

            foreach ($occurrences as $file => $paths) {
                foreach ($paths as $path) {
                    $path = trim($path, '/');
                    $path_parts = ($path == '') ? array() : explode('/', $path);

                    $files_map[$file][] = $path_parts;
                }
            }

            $ftw->get_cache()->put($this->_storage_key . $tag, $files_map);
        }

        foreach ($files_map as $file => $paths) {
            foreach ($paths as $path_parts) {
                if (count($path_parts) != count($uri_parts)) {
                    continue;
                }

                $match = true;

                for ($i = 0; $i < count($path_parts); $i++) {
                    if (substr($path_parts[$i], 0, 1) == ':') {
                        $args[substr($path_parts[$i], 1)] = $uri_parts[$i];

                        continue;
                    }

                    if ($uri_parts[$i] == $path_parts[$i]) {
                        continue;
                    }

                    $match = false;
                    break;
                }

                if ($match) {
                    return array($file, $args);
                }
            }
        }

        return false;
    }
}