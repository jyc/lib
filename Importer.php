<?php
/*
 * Copyright (c) 2011, Jonathan Chan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 * following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace jonathanyc\lib;

/**
 * A simple "Importer". An instance is intended to be passed around to all
 * files in an application, so that a common $base_dir can be established.
 */
class Importer
{

    /**
     * The root from which modules should be loaded.
     * @var string
     */
    private $_base_dir;

    /**
     * An array of already loaded files and their return values.
     * @var array
     */
    private $_already_loaded;

    /**
     * Instantiates a new Importer instance and establishes its base directory
     * at the supplied $base_dir.
     * @param string $base_dir The base directory from which all modules should be
     * loaded.
     * @throws \InvalidArgumentException if the supplied $base_dir does not exist.
     */
    public function __construct($base_dir = '.')
    {
        if (!is_dir($base_dir)) {
            throw new \InvalidArgumentException("The supplied \$base_dir does not"
                    . " exist!");
        }

        // Normalize the base_dir ending to have a '/'
        $this->_base_dir = rtrim($base_dir, '/') . '/';
    }

    /**
     * Attempts to load the given module(s), which are simply identified by
     * PHP files without a .php at the end, based on the base_dir.
     * @param string|array $module The module name(s) to load. If a module name
     * identifies a directory, we will attempt to load a file named main.php.
     * @return mixed|null
     * @throws \InvalidArgumentException if a module file does not exist.
     */
    public function load($module)
    {
        // If the file has already been loaded, just return the return value
        if (isset($this->_already_loaded[$module])) {
            return $this->_already_loaded[$module];
        }

        if (is_array($module)) {
            foreach ($module as $each) {
                $this->load($each);
            }

            return null;
        }

        if (is_dir($this->_base_dir . $module)) {
            if (file_exists($this->_base_dir . $module . '/main.php')) {
                return require($this->_base_dir . $module . '/main.php');
            }
        }

        if (!file_exists($this->_base_dir . $module . '.php')) {
            throw new \InvalidArgumentException("The file " . $module . " does not"
                    . " exist!");
        }

        return require($this->_base_dir . $module . '.php');
    }
}