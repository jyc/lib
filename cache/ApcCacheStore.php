<?php
/*
 * Copyright (c) 2011, Jonathan Chan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 * following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace jonathanyc\lib\cache;

/**
 * A "no-setup" implementation of CacheStore that stores data in the APC cache.
 */
class ApcCacheStore implements CacheStore
{
    /**
     * The value with which stored keys will be prefixed with.
     * @var string
     */
    public $_prefix;

    /**
     * Constructs a new instance.
     * @param array $config A configuration array for the ApcCacheStore, with the
     * following required keys:
     * <table>
     *     <tr><th>Key name</th><th>Short Description</th></tr>
     *     <tr><td>prefix</td><td>The value to prefix stored keys with.</td></tr>
     * </table>
     * @throws InvalidArgumentException
     */
    public function __construct(array $config = array())
    {
        if (!extension_loaded('apc')) {
            throw new \InvalidArgumentException("The APC extension is not loaded!");
        }

        // Default $config['prefix'] to ''
        $this->_prefix = isset($config['prefix']) ? $config['prefix'] : '';
    }

    /**
     * @see jonathanyc\lib\cache.CacheStore::mtime()
     */
    public function mtime($key)
    {
        $result = apc_fetch($this->_prefix . $key);

        if ($result === false) return null;

        list($time, $data) = $result;

        return $time;
    }

    /**
     * @see jonathanyc\lib\cache.CacheStore::put()
     */
    public function put($key, $data)
    {
        return apc_store($this->_prefix . $key, array(time(), $data));
    }

    /**
     * @see jonathanyc\lib\cache.CacheStore::get()
     */
    public function get($key)
    {
        $result = apc_fetch($this->_prefix . $key);

        if ($result === false) return null;

        list($time, $data) = $result;

        return $data;
    }

    /**
     * @see jonathanyc\lib\cache.CacheStore::delete()
     */
    public function delete($key)
    {
        return apc_delete($this->_prefix . $key);
    }
}