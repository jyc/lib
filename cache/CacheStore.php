<?php
/*
 * Copyright (c) 2011, Jonathan Chan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 * following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace jonathanyc\lib\cache;

/**
 * An interface for objects which are usable as a cache store - basically a
 * simple key-value store.
 */
interface CacheStore {
    /**
     * Should construct a new implementation of CacheStore.
     * @param array $config An implementation-specific associative array of
     * configuration keys and their values.
     */
    public function __construct(array $config);

    /**
     * Should return the last time the data in $key was modified.
     * @param string $key
     * @return bool|string The last time the data in $key was modified, null if no record with
     * key $key exists, or  false on failure.
     */
    public function mtime($key);

    /**
     * Should store the data in $data so that it is accessible by the given $key.
     * @param string $key
     * @param mixed $data The data to store. Any PHP type, except for an object
     * or a resource, must be supported.
     * @return bool True on success, false on failure.
     */
    public function put($key, $data);

    /**
     * Should retrieve the data stored with the key $key.
     * @param string $key
     * @return mixed|null The value if succesful, or null on failure.
     */
    public function get($key);

    /**
     * Should delete the entry with the key $key.
     * @param string $key
     * @return bool True on success, false on failure.
     */
    public function delete($key);
}