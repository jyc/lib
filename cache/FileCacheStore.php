<?php
/*
 * Copyright (c) 2011, Jonathan Chan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 * following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace jonathanyc\lib\cache;

/**
 * A simple file-based implementation of CacheStore that stores data as files in
 * a given cache directory, encoded in JSON and with the key being the file
 * name.
 */
class FileCacheStore implements CacheStore
{
    /**
     * The path in which to store the cache data - a directory.
     * @var string
     */
    public $_path;

    /**
     * Constructs a new instance.
     * @param array $config A configuration array for the FileCacheStore, with the
     * following required keys:
     * <table>
     *     <tr><th>Key name</th><th>Short Description</th></tr>
     *     <tr><td>path</td><td>The path in which to store the cache data.</td></tr>
     * </table>
     * @throws \InvalidArgumentException
     */
    public function __construct(array $config)
    {
        if (!isset($config['path'])) {
            throw new \InvalidArgumentException("The 'path' configuration key is"
                    . " missing!");
        }

        // Create the cache directory if it does not exist
        if (!is_dir($config['path']))
        {
            if (!mkdir($config['path'], 777)) {
                throw new \InvalidArgumentException("A directory could not be created at the location supplied in the"
                        . " 'path' key!");
            }
        }

        // Remove extraneous trailing slashes if they exist, then add one (to make
        // sure we end up with a name that we can easily concatenate)
        $this->_path = rtrim($config['path'], '/') . '/';
    }

    /**
     *
     * @see jonathanyc\lib\ftw.CacheStore::mtime()
     */
    public function mtime($key)
    {
        $file_name = $this->_path . md5($key);

        return @file_exists($file_name) ?
                filemtime($file_name) : null;
    }

    /**
     *
     * @see jonathanyc\lib\ftw.CacheStore::put()
     */
    public function put($key, $data)
    {
        $json = json_encode($data);

        // Open the file in binary mode (for portability) and for (over)writing
        $fh = @fopen($this->_path . md5($key), 'bw');

        // If we failed to open the file, return false.
        if ($fh === false) {
            return false;
        }

        // Write the data, and return false if we failed.
        if (fwrite($fh, $json) === false) {
            return false;
        }

        fclose($fh);

        return true;
    }

    /**
     * @see jonathanyc\lib\ftw.CacheStore::get()
     */
    public function get($key)
    {
        $file_name = $this->_path . md5($key);

        $fh = @fopen($file_name, 'br');

        if ($fh === false) {
            return null;
        }

        $json = fgets($fh, filesize($file_name));

        if ($json === false) {
            return null;
        }

        return json_decode($json);
    }

    /**
     * @see jonathanyc\lib\cache.CacheStore::delete()
     */
    public function delete($key)
    {
        return @unlink($this->_path . md5($key));
    }
}
