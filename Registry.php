<?php
/*
 * Copyright (c) 2011, Jonathan Chan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 * following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace jonathanyc\lib;

/**
 * An boilerplate implementation of the Registry pattern.
 * @link http://martinfowler.com/eaaCatalog/registry.html
 */
class Registry implements \Countable
{
    protected $_registry;

    public function __set($key, $value)
    {
        $this->set($key, $value);
    }

    public function __get($key)
    {
        return $this->get($key);
    }

    public function __isset($key)
    {
        return isset($this->_registry[$key]);
    }

    public function __unset($key)
    {
        unset($this->_registry[$key]);
    }

    public function set($key, $value)
    {
        $this->_registry[$key] = $value;
    }

    /**
     * Stores a value in the Registry, returning null if there is no value
     * with that key.
     * @param mixed $key
     * @return mixed|null
     */
    public function get($key)
    {
        if (!@isset($this->_registry[$key])) {
            return null;
        }

        return $this->_registry[$key];
    }

    public function delete($key)
    {
        unset($this->_registry[$key]);
    }

    public function count()
    {
        return count($this->_registry);
    }
}